from math import pi
from numpy import array, cos, sin
def wrap_orientation(yaw, zeroto2pi = False):
    if not zeroto2pi:
        while(yaw > pi):
            yaw = yaw - 2*pi
        while(yaw < -pi):
            yaw = yaw + 2*pi
    else:
        while (yaw > 2*pi):
            yaw = yaw - 2*pi
        while(yaw < 0):
            yaw = yaw + 2*pi

    return(yaw)

def rotate_vector(x, y, yaw):
    R = array([[cos(yaw), -sin(yaw)], [sin(yaw), cos(yaw)]])
    vector = array([x, y])
    vector_rotated = R.dot(vector)
    return vector_rotated