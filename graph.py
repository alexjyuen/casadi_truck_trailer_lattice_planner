from node import Node
from math import fabs, sqrt, isinf
from matplotlib import pyplot as plt
import time
from utils import wrap_orientation
class Graph():
    def __init__(self):
        self.Nodes = []
        self.index = 0
        self.open_list = []
        self.closed_list = []
        self.visited_list = []
        self.unvisited_list = []
        self.path_list = []
        self.N = 0
        self.g = 0.0

    def dfs(self, start_node, end_node):
        if start_node.index is end_node.index:
            return True

        self.visited_list.append(start_node.index)
        if(start_node.x == 4.0) and (start_node.y == 4.0):
            print("x: " + str(start_node.x) + " y: " + str(start_node.y) + " yaw: " + str(start_node.yaw))

        self.N = self.N+1
        for i in range(0, len(start_node.children)):
            if not (start_node.children[i] in self.visited_list):
                node_found = self.dfs(self.Nodes[start_node.children[i]], end_node)
                if(node_found):
                    self.path_list.append(start_node.children[i])
                    return True

        return False

    def astar(self, x_0, y_0, yaw_0, alpha_0, x_f, y_f, yaw_f, alpha_f, control_sequences):
        self.visited_list = []
        self.unvisited_list = []
        self.path_list = []
        self.dijkstra(x_0, y_0, yaw_0, alpha_0, x_f, y_f, yaw_f, alpha_f, control_sequences, 1)

    def dijkstra(self, x_0, y_0, yaw_0, alpha_0, x_f, y_f, yaw_f, alpha_f, control_sequences, enable_astar = 0):
        self.visited_list = []
        self.unvisited_list = []
        self.path_list = []

        start_node = Node(x_0, y_0, yaw_0, alpha_0)
        start_node.cost = 0.0
        # self.unvisited_list.append(start_node)
        self.add_node(start_node)
        self.find_shortest_path(start_node, x_f, y_f, yaw_f, alpha_f, enable_astar, control_sequences)


    def find_shortest_path(self, node, x_f, y_f, yaw_f, alpha_f, enable_astar, controlsequences):

        for controlsequence in controlsequences:
            if fabs(controlsequence.theta[0] - node.yaw) < 1e-9 and fabs(controlsequence.alpha[0] - node.alpha) < 1e-9: #oriented the same way
                child_x = node.x + controlsequence.x[-1]
                child_y = node.y + controlsequence.y[-1]
                child_yaw = controlsequence.theta[-1]
                child_alpha = controlsequence.alpha[-1]

                plt.plot(node.x + controlsequence.x, node.y + controlsequence.y, color='blue')

                dx = x_f - child_x
                dy = y_f - child_y
                h = sqrt(dx ** 2 + dy ** 2) * enable_astar
                cost = node.cost + controlsequence.s + h
                if(controlsequence.v < 0):
                    cost = cost + 0*100

                # if child_x > 20 and child_x < 80 and child_y > 20 and child_y < 80:
                #     cost = float('inf')

                child_node = Node(child_x, child_y, child_yaw, child_alpha, node.index)

                child_index = self.add_node(child_node, controlsequence.s, controlsequence.v, controlsequence.u,
                                            controlsequence.T, node.x + controlsequence.x, node.y + controlsequence.y, controlsequence.alpha)

                if cost < self.Nodes[child_index].cost:
                    self.Nodes[child_index].cost = cost
                    self.Nodes[child_index].previous_node_index = node.index


                if fabs(child_x-x_f) < 1e-10 and fabs(child_y - y_f)< 1e-10 and fabs(child_yaw - yaw_f)< 1e-10 and fabs(child_alpha - alpha_f)< 1e-10:
                    print("End Node found!")
                    return True

        self.unvisited_list.remove(node)
        self.visited_list.append(node)
        min_index = self.get_min_cost()


        found_node = self.find_shortest_path(self.Nodes[min_index], x_f, y_f, yaw_f, alpha_f, enable_astar, controlsequences)
        if (found_node):
            self.path_list.append(min_index)
            return True
        else:
            return False

    def get_min_cost(self):
        min_cost = float('inf')
        min_index = float('inf')
        for index, node in enumerate(self.unvisited_list):
            if node.cost < min_cost:
                min_cost = node.cost
                min_index = node.index

        if isinf(min_index):
            print(len(self.unvisited_list))

        return min_index

    def add_node(self, Node, s=0.0, v=0.0, u=0.0, T = 0.0, x=0.0, y=0.0, alpha=0.0):

        node_exists = False

        for node in self.Nodes:
            ex = node.x - Node.x
            ey = node.y - Node.y
            eyaw = node.yaw - Node.yaw
            ealpha = node.alpha - Node.alpha

            if (fabs(eyaw) > 6):
                Node.yaw = node.yaw
                eyaw = 0.0

            if fabs(ex) < 0.1 and fabs(ey) < 0.1 and fabs(eyaw) < 0.1 and fabs(ealpha) < 0.1:
                Node.index = node.index
                node_exists = True
                break

        if not node_exists:
            Node.index = self.index
            self.Nodes.append(Node)
            self.unvisited_list.append(Node)
            self.index = self.index + 1

        for node in self.Nodes:
            if node.index == Node.parent:
                node.add_child(Node.index, s, v, u, T, x, y, alpha)

        return Node.index

    def find_closest_node(self, x, y, yaw, alpha):
        ex = self.Nodes[0].x - x
        ey = self.Nodes[0].y - y
        eyaw = self.Nodes[0].yaw - yaw
        ealpha = self.Nodes[0].alpha - alpha

        error_0 = sqrt(ex**2+ey**2+eyaw**2+ealpha**2)
        index = self.Nodes[0].index

        for node in self.Nodes:
            ex = node.x - x
            ey = node.y - y
            eyaw = node.yaw - yaw
            ealpha = node.alpha - alpha
            error = sqrt(ex**2+ey**2+eyaw**2+ealpha**2)

            if(error < error_0):
                error_0 = error
                index = node.index

        return index, error_0

