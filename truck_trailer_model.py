import config as config  # simple convenience, used to store config values
import numpy as np
import math
from matplotlib import pyplot as plt
from math import cos, sin, tan, fabs, atan, sqrt, copysign

class TruckTrailerModel:

    def __init__(self):

        # Static values
        self._length_in_front_of_axle = np.array(config.length_in_front_of_axle)
        self._L = np.array(config.length_in_front_of_axle)
        self._length_behind_axle = np.array(config.length_behind_axle)
        self._axle_length = np.array(config.axle_length)

        self._n_axles = np.size(self._axle_length)+1

        # Vehicle states
        self._x = config.x0
        self._y = config.y0
        # self._theta = np.array(config.theta0)
        self._theta = np.array([0,0,0])
        self._beta = np.array(config.beta0)
        self._alpha = np.array(config.alpha0)
        self._omega = np.array(config.omega0)

        self._states = np.array([self._x, self._y])
        self._states = np.append(self._states, 0)
        self._states = np.append(self._states, self._beta)
        self._states = np.append(self._states, self._alpha)
        self._states = np.append(self._states, self._omega)
        self._d_states = np.zeros(np.size(self._states))

        self._wheel_radius = 0.25
        self._Ts = 0.1
        self.v_1 = 20

        #Curve parameters
        self._n_points = 5000
        self._R = 10
        self._s = np.linspace(0, 2*math.pi*self._R, self._n_points)
        self._s = self._s[0:self._n_points-1]
        self._curve_x = self._R*np.cos(self._s/self._R)
        self._curve_y = self._R*np.sin(self._s/self._R)
        self._velocity = np.zeros(self._n_axles)

        #Control states
        self._frenet_frames = np.zeros(self._n_axles, int)
        self._z = np.zeros(self._n_axles)
        self._dz = np.zeros(self._n_axles)

        self._axle_positions = np.zeros([self._n_axles, 2], dtype='f')

        self._direction = 1.0

    def plot_axle(self, R, axle_position, index, ax, axis_id, steer_angle = 0):
        R_steer = self.rot_matrix(steer_angle)

        left_axle_position = R.dot(np.array([0, self._axle_length[index] / 2])) + axle_position
        left_wheel_position_p = R_steer.dot(R.dot(np.array([self._wheel_radius, 0])))+ left_axle_position
        left_wheel_position_n = R_steer.dot(R.dot(np.array([-self._wheel_radius, 0]))) + left_axle_position

        right_axle_position = R.dot(np.array([0, -self._axle_length[index] / 2])) + axle_position
        right_wheel_position_p = R_steer.dot(R.dot(np.array([self._wheel_radius, 0]))) + right_axle_position
        right_wheel_position_n = R_steer.dot(R.dot(np.array([-self._wheel_radius, 0]))) + right_axle_position

        axis_id[0], = ax.plot([left_wheel_position_n[0], left_wheel_position_p[0]], [left_wheel_position_n[1], left_wheel_position_p[1]], 'k')
        axis_id[1], = ax.plot([right_wheel_position_n[0], right_wheel_position_p[0]], [right_wheel_position_n[1], right_wheel_position_p[1]], 'k')
        axis_id[2], = ax.plot([left_axle_position[0], right_axle_position[0]], [left_axle_position[1], right_axle_position[1]], 'k')

    def set_simulation(self, ref_x, ref_y, u, dt):
        self.ref_x = ref_x
        self.ref_y = ref_y
        self.steering_angle = u
        self._Ts = dt

    def plot_vehicle(self, steer_angle):
        R = self.rot_matrix(self._theta[0])
        axle_position = np.array([self._x, self._y])
        front_position = R.dot(np.array([self._length_in_front_of_axle[0], 0])) + axle_position
        rear_position = R.dot(np.array([-self._length_behind_axle[0], 0])) + axle_position

        self._axle_positions[0] = front_position
        self._axle_positions[1] = axle_position

        self.plot_axle(R, front_position, 0, plt, steer_angle)
        self.plot_axle(R, axle_position, 0, plt)

        plotting_points = np.array(front_position)
        plotting_points = np.vstack([plotting_points, axle_position])
        plotting_points = np.vstack([plotting_points, rear_position])


        for trailer_index, length in enumerate(self._length_in_front_of_axle[1:np.size(self._length_in_front_of_axle)]):
            R = self.rot_matrix(self._theta[trailer_index+1])
            axle_position = plotting_points[-1,:] - R.dot(np.array([self._length_in_front_of_axle[trailer_index+1], 0]))
            self._axle_positions[trailer_index + 2] = axle_position
            rear_position = axle_position - R.dot(np.array([self._length_behind_axle[trailer_index+1], 0]))

            self.plot_axle(R, axle_position, trailer_index + 1, plt)

            plotting_points = np.vstack([plotting_points, axle_position])
            plotting_points = np.vstack([plotting_points, rear_position])

        plt.title("Truck Trailer model")
        plt.xlabel("X[m]")
        plt.ylabel("Y[m]")
        plt.plot(plotting_points[:,0], plotting_points[:,1], 'k')
        plt.plot(plotting_points[:, 0], plotting_points[:, 1],'.')
        plt.plot(self._curve_x, self._curve_y)
        plt.axis('scaled')
        plt.xlim([-20, 20])
        plt.ylim([-20, 20])

    def rot_matrix(self, theta):
        c, s = np.cos(theta), np.sin(theta)
        R = np.array(((c, -s), (s, c)))
        return R

    def get_differential_states(self, v, u):
        self._d_states = self.get_d_states(self._states, v, u)

    def get_d_states(self, states, v, u):

        theta3 = states[2]
        beta3 = states[3]
        beta2 = states[4]
        alpha = states[5]
        omega = states[6]
        # alpha = u

        M1 = self._length_behind_axle[0]
        L1 = self._length_in_front_of_axle[0]
        L2 = self._length_in_front_of_axle[1]
        L3 = self._length_in_front_of_axle[2]

        d_states = np.zeros(np.size(self._states))

        d_states[0] = v * cos(beta3) * cos(beta2) * (1 + M1 / L1 * tan(beta2) * tan(alpha)) * cos(theta3)
        d_states[1] = v * cos(beta3) * cos(beta2) * (1 + M1 / L1 * tan(beta2) * tan(alpha)) * sin(theta3)
        d_states[2] = v * ((sin(beta3) * cos(beta2)) / L3) * (1 + M1 / L1 * tan(beta2) * tan(alpha))
        d_states[3] = v * cos(beta2) * ((1 / L2) * (tan(beta2) - (M1 / L1) * tan(alpha)) - (sin(beta3) / L3) * (1 + (M1 / L1) * tan(beta2) * tan(alpha)))
        d_states[4] = v * (tan(alpha) / L1 - sin(beta2) / L2 + M1 / (L1 * L2) * cos(beta2) * tan(alpha))
        d_states[5] = omega
        d_states[6] = u

        return d_states
    def runge_kutta(self, v, u):
        k1 = self.get_d_states(self._states, v, u)
        k2 = self.get_d_states((self._states + k1 * self._Ts / 2), v, u)
        k3 = self.get_d_states((self._states + k2 * self._Ts / 2), v, u)
        k4 = self.get_d_states((self._states + k3 * self._Ts), v, u)

        return self._states + (self._Ts/6)*(k1 + 2*k2 + 2*k3 + k4)

    def forward_euler(self, v, u, plot_flag = False, ax = 0):
        #self._states = self._states + self.get_d_states(self._states, v, u) * self._Ts

        self._states = self.runge_kutta(v, u)

        M1 = self._length_behind_axle[0]
        L1 = self._length_in_front_of_axle[0]
        L2 = self._length_in_front_of_axle[1]
        L3 = self._length_in_front_of_axle[2]

        x3 = self._states[0]
        y3 = self._states[1]
        theta3 = self._states[2]
        beta3 = self._states[3]
        beta2 = self._states[4]

        x2 = L3*cos(theta3) + x3
        y2 = L3*sin(theta3) + y3
        theta2 = theta3+beta3

        x1 = L2*cos(theta2) + x2
        y1 = L2*sin(theta2) + y2
        theta1 = theta2+beta2

        x0 = (M1)*cos(theta1) + x1
        y0 = (M1)*sin(theta1) + y1

        xf = L1*cos(theta1) + x0
        yf = L1*sin(theta1) + y0

        x = [xf, x0, x1, x2, x3]
        y = [yf, y0, y1, y2, y3]

        self._theta[0] = theta1
        self._theta[1] = theta2
        self._theta[2] = theta3
        self._x = x0
        self._y = y0
        # self.plot_vehicle(u)

        # print(x)
        # print(y)
        # #
        if plot_flag:
            plt.plot(self.ref_x, self.ref_y, label="Reference Path")
            # plt.legend()
            # chassis, = ax.plot(x,y, color='black')
            plt.plot(x, y, color='black')
            self.axle_1 = [None] * 3
            self.axle_2 = [None] * 3
            self.axle_3 = [None] * 3
            self.axle_4 = [None] * 3
            R = self.rot_matrix(theta3)
            self.plot_axle(R, np.array([x3, y3]), 2, plt, self.axle_1)
            R = self.rot_matrix(theta2)
            self.plot_axle(R, np.array([x2, y2]), 2, plt, self.axle_2)
            R = self.rot_matrix(theta1)
            self.plot_axle(R, np.array([x0, y0]), 2, plt, self.axle_3)
            R = self.rot_matrix(theta1)
            self.plot_axle(R, np.array([xf, yf]), 2, plt, self.axle_4, u)
            plt.axis('scaled')
            # plt.xlim([0, 80])
            # plt.ylim([-10, 40])
            plt.show(block=False)
            plt.pause(0.0001)
            plt.clf()

            # chassis.remove()
            # for i in range(0,3):
            #     self.axle_1[i].remove()
            #     self.axle_2[i].remove()
            #     self.axle_3[i].remove()
            #     self.axle_4[i].remove()

            # for i in range(0,5):
            #     ax.pop(i)

            # plt.clf()

if __name__ == '__main__':

    model = TruckTrailerModel()
    alpha_e = 0.3
    M1 = model._length_behind_axle[0]
    L1 = model._length_in_front_of_axle[0]
    L2 = model._length_in_front_of_axle[1]
    L3 = model._length_in_front_of_axle[2]

    R1 = L1/fabs(tan(alpha_e))
    R2 = sqrt(R1**2 + M1**2 - L2**2)
    R3 = sqrt(R2**2 - L3**2)

    beta3_e = copysign(1,alpha_e)*atan(L3/R3)
    beta2_e = copysign(1,alpha_e)*(atan(M1/R1) + atan(L2/R2))

    print("length of time vector: " + str(len(model.t)))
    input("pause")

    for i, u in enumerate(model.steering_angle):
        model.forward_euler(1.0, u, model.steering_angle[i+1])
        print("beta3: " + str(model._states[3]) + " ,beta2: " + str(model._states[4]))
        print("beta3_e: " + str(beta3_e) + " ,beta2: " + str(beta2_e))

