import numpy
import math
from math import pi
from matplotlib import pyplot as plt

class ControlSequence():
    def __init__(self, N = 0):
        self.x = numpy.zeros(N)
        self.y = numpy.zeros(N)
        self.theta = numpy.zeros(N)
        self.alpha = numpy.zeros(N)
        self.yaw = numpy.zeros(N)
        self.p = numpy.zeros(N)
        self.u = numpy.zeros(N)
        self.s = 0.0
        self.v = 0.0
        self.T = 0.0

if __name__ == '__main__':
    control_sequence = ControlSequence()