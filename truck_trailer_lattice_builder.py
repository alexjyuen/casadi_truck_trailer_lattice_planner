from truck_trailer_optimizer import TruckTrailerOptimizer
from truck_trailer_model import TruckTrailerModel
from math import pi, cos, sin, atan, fabs, sqrt, tan, copysign, isnan
from matplotlib import pyplot as plt
import numpy as np
import time
from multiprocessing import Pool
from controlsequence import ControlSequence
from graph import Graph
from utils import wrap_orientation
from node import Node

M1 = 0.8
L1 = 4.66
L2 = 3.75
L3 = 7.59
N = 100
beta3_max = pi/2
beta3_min = -pi/2
beta2_max = pi/2
beta2_min = -pi/2
alpha_max = pi/4
alpha_min = -pi/4
omega_max = 1.5
omega_min = -1.5
u_max = 40.0
u_min = -40.0
controlsequences = []
graph = Graph()

truck_trailer_optimizer = TruckTrailerOptimizer(M1, L1, L2, L3, N, beta3_max, beta3_min, beta2_max, beta2_min, alpha_max, alpha_min, omega_max, omega_min, u_max, u_min)

beta3_e, beta2_e = truck_trailer_optimizer.get_beta(0)
beta3_s, beta2_s = truck_trailer_optimizer.get_beta(0)

def optimize_parallel(x_s, y_s, theta_s, alpha_s, x_e, y_e, theta_e, alpha_e):
    solve_flag, ref_x, ref_y, ref_theta, u, T = truck_trailer_optimizer.run_optimizer(x_s, y_s, theta_s, alpha_s, x_e, y_e, theta_e, alpha_e)
    return solve_flag, ref_x, ref_y, ref_theta, u, T

def optimize_wrapper(args):
    x_s, y_s, theta_s, alpha_s, x_e, y_e, theta_e, alpha_e = args
    return optimize_parallel(x_s, y_s, theta_s, alpha_s, x_e, y_e, theta_e, alpha_e)


optimize_args = []
alpha = [-0.2117, 0, 0.2117]
alpha = [0.0]
# theta = [-pi/2, -pi/4, 0, pi/4, pi/2]

theta = [0, pi/4, pi/2]

M = 1
start = 0.0
end = 15.0
ds = (end-start)/M

Y = np.arange(start, end+ds, ds)
X = [ds]

for x_s in X:
    for y_s in Y:
        for theta_s in theta:
            for alpha_s in alpha:
                for x_e in X:
                    for y_e in Y:
                        for theta_e in theta:
                            for alpha_e in alpha:
                                optimize_args.append((0.0, 0.0, theta_s, alpha_s, x_e, y_e, theta_e, alpha_e))

# optimize_args.append((0.0, 0.0, pi/4, 0.0, 20.0, 20.0, pi/4, 0.0))

# print(len(optimize_args))
#
# input("PAUSE")

start = time.time()
# Create a Pool object to manage a pool of worker processes
with Pool(20) as pool:
    # Perform roll-outs in parallel and collect results
    results = pool.map(optimize_wrapper, optimize_args)
end = time.time()
print("elapsed time: " + str(end-start))

for i, (solve_flag, ref_x, ref_y, ref_theta, u, T) in enumerate(results):
    print("index i: " + str(i))

    if solve_flag:
        dt = T / N
        print(dt)
        plt.plot(ref_x, ref_y, color='blue')
        controlsequence = ControlSequence(N)
        controlsequence.x = ref_x
        controlsequence.y = ref_y

        diff_x = np.diff(ref_x)
        diff_y = np.diff(ref_y)

        diff_x_2 = np.power(diff_x, 2)
        diff_y_2 = np.power(diff_y, 2)

        controlsequence.s = np.sum(np.square(np.add(diff_x_2, diff_y_2)))


        controlsequence.theta = ref_theta
        controlsequence.u = u
        controlsequence.T = T
        controlsequences.append(controlsequence)

print(len(controlsequences))
plt.show()

# for controlsequence in controlsequences:
#     print(controlsequence.x[-1])
#     print(controlsequence.y[-1])
#     print(controlsequence.theta[-1])

n_grid = 2
x_lattice = range(-n_grid * int(ds), (n_grid + 1) * int(ds), int(ds))
y_lattice = x_lattice
yaw_lattice = [-pi / 2, 0, pi / 2, pi]

for x in x_lattice:
    for y in y_lattice:
        for yaw in yaw_lattice:
            for controlsequence in controlsequences:
                node_exists = False
                dx = controlsequence.x[-1]
                dy = controlsequence.y[-1]
                theta_s = controlsequence.theta[0] + yaw
                theta_e = controlsequence.theta[-1] + yaw
                for node in graph.Nodes:
                    if node.x == x and node.y == y and node.yaw == theta_s:
                        node_exists = True
                        break

                if not node_exists:
                    node = Node(x, y, theta_s, 0.0)
                    graph.add_node(node)

                yaw_child = wrap_orientation(theta_e)
                x_child = node.x + dx * cos(node.yaw) - dy * sin(node.yaw)
                y_child = node.y + dx * sin(node.yaw) + dy * cos(node.yaw)

                child_node = Node(x_child, y_child, yaw_child, 0.0, node.index)
                graph.add_node(child_node, 1.0, 1.0, controlsequence.u, controlsequence.T)

for i in range(0, len(graph.Nodes)):
    for j in range(0, len(graph.Nodes[i].children)):
        model = TruckTrailerModel()
        beta3_s, beta2_s = truck_trailer_optimizer.get_beta(graph.Nodes[i].alpha)
        model._states[0] = graph.Nodes[i].x
        model._states[1] = graph.Nodes[i].y
        model._states[2] = graph.Nodes[i].yaw
        model._states[3] = beta3_s
        model._states[4] = beta2_s
        model._states[5] = graph.Nodes[i].alpha
        u = graph.Nodes[i].edges[j].u
        dt = graph.Nodes[i].edges[j].T / N
        model.set_simulation(0.0, 0.0, u, dt)
        controlsequence = ControlSequence(N)
        for k in range(0, N):
            model.forward_euler(1.0, u[k])
            controlsequence.x[k] = model._states[0]
            controlsequence.y[k] = model._states[1]

        plt.plot(controlsequence.x, controlsequence.y, color='r')

plt.show()
# n_grid = 6
# x_lattice = range(-n_grid * 10, (n_grid + 1) * 10, 10)
# y_lattice = x_lattice
#
# yaw_lattice = [-pi / 2, 0, pi / 2, pi]
#
# dy = [-20.0, -20.0, -20.0, 0.0, 0.0, 0.0, 20.0, 20.0, 20.0]
# dyaw = [-pi / 2, 0, pi / 2, -pi / 2, 0, pi / 2, -pi / 2, 0, pi / 2]
#
# for x in x_lattice:
#     for y in y_lattice:
#         for yaw in yaw_lattice:
#             node_exists = False
#             for node in self.graph.Nodes:
#                 if node.x == x and node.y == y and node.yaw == yaw:
#                     node_exists = True
#                     break
#
#             if not node_exists:
#                 node = Node(x, y, yaw, 0.0)
#                 self.graph.add_node(node)
#
#             dx = 20.0
#
#             for i in range(0, 9):
#                 yaw_child = node.yaw + dyaw[i]
#                 yaw_child = wrap_orientation(yaw_child)
#
#                 x_child = node.x + dx * cos(node.yaw) - dy[i] * sin(node.yaw)
#                 y_child = node.y + dx * sin(node.yaw) + dy[i] * cos(node.yaw)
#
#                 child_node = Node(x_child, y_child, yaw_child, 0.0, node.index)
#                 self.graph.add_node(child_node, 1.0, 1.0, self.controlsequences[i].u, self.controlsequences[i].T)
