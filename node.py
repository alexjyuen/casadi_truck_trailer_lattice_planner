import numpy
import math
class Edge():
    u = numpy.empty
    s = 0.0
    v = 0.0
    i = 0
    T = 0.0

class Node():
    def __init__(self, x = 0, y = 0, yaw = 0, alpha = 0, parent = float('nan'), x_index = 0, y_index = 0, yaw_index = 0):
        self.index = 0
        self.x = x
        self.y = y
        self.x_index = x_index
        self.y_index = y_index
        self.yaw_index = yaw_index
        self.x_list = []
        self.y_list = []
        self.yaw_list = []

        self.yaw = yaw
        self.alpha = alpha
        self.parent = parent
        self.children = []
        self.edges = []
        self.cost = float('inf')
        self.previous_node_index = float('nan')

    def add_child(self, i, s, v, u, T, x, y, alpha):
        self.children.append(i)
        edge = Edge()
        edge.s = s
        edge.v = v
        edge.u = u
        edge.i = i
        edge.x = x
        edge.y = y
        edge.alpha = alpha
        edge.T = T
        self.edges.append(edge)