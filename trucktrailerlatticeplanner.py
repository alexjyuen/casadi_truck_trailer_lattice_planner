import numpy
from numpy import zeros, array
from truck_trailer_optimizer import TruckTrailerOptimizer
from math import pi, sqrt, floor, cos, sin, isnan, fabs
from matplotlib import pyplot as plt
from controlsequence import ControlSequence
import matplotlib
from scipy.interpolate import interp1d
from node import Node
from graph import Graph
from utils import wrap_orientation
import numpy as np
import time
import pickle
from multiprocessing import Pool
from truck_trailer_model import TruckTrailerModel
from matplotlib.patches import Polygon

class TruckTrailerLatticePlanner():
    def __init__(self):
        self.controlsequences = []
        self.graph = Graph()
        self.v = 1.0
        self.Ts = 0.01
        self.index_end = 0

        self.M1 = 0.8
        self.L1 = 4.66
        self.L2 = 3.75
        self.L3 = 7.59
        self.N = 75
        self.beta3_max = pi / 2
        self.beta3_min = -pi / 2
        self.beta2_max = pi / 2
        self.beta2_min = -pi / 2
        self.alpha_max = pi / 4
        self.alpha_min = -pi / 4
        self.omega_max = 1.5
        self.omega_min = -1.5
        self.u_max = 40.0
        self.u_min = -40.0
        self.index_end = 0
        self.optimal_node_traversal = []
        self.optimal_x = np.empty(0)
        self.optimal_y = np.empty(0)

        self.optimizer = TruckTrailerOptimizer(self.M1, self.L1, self.L2, self.L3, self.N, self.beta3_max, self.beta3_min,
                                          self.beta2_max, self.beta2_min,
                                          self.alpha_max, self.alpha_min, self.omega_max, self.omega_min, self.u_max,
                                          self.u_min)

    def run_model(self, kinematics, v, w, N, Ts):
        for j in range(0, N):
            kinematics.forward_kinematic(v, w(j * Ts), 1)

        return kinematics

    def optimize_parallel(self, x_s, y_s, theta_s, alpha_s, x_e, y_e, theta_e, alpha_e, optimizer):
        solve_flag, ref_x, ref_y, ref_theta, ref_alpha, u, T = optimizer.run_optimizer(x_s, y_s, theta_s, alpha_s, x_e, y_e,
                                                                               theta_e, alpha_e)
        return solve_flag, ref_x, ref_y, ref_theta, ref_alpha, u, T

    def optimize_wrapper(self, args):
        x_s, y_s, theta_s, alpha_s, x_e, y_e, theta_e, alpha_e, optimizer = args
        return self.optimize_parallel(x_s, y_s, theta_s, alpha_s, x_e, y_e, theta_e, alpha_e, optimizer)

    def check_for_duplicates(self, test_controlsequence, controlsequences):
        duplicate = False
        for controlsequence in controlsequences:
            if(test_controlsequence.v == controlsequence.v and fabs(fabs(test_controlsequence.s-controlsequence.s)) < 1e-5):
                if (fabs(test_controlsequence.x[0] - controlsequence.x[0]) < 1e-10 and
                    fabs(test_controlsequence.x[-1] - controlsequence.x[-1]) < 1e-10 and
                    fabs(test_controlsequence.y[0] - controlsequence.y[0]) < 1e-10 and
                    fabs(test_controlsequence.y[-1] - controlsequence.y[-1]) < 1e-10 and
                    fabs(test_controlsequence.theta[0] - controlsequence.theta[0]) < 1e-10 and
                    fabs(test_controlsequence.theta[-1] - controlsequence.theta[-1]) < 1e-10):
                        x_norm = np.linalg.norm(test_controlsequence.x - controlsequence.x)
                        y_norm = np.linalg.norm(test_controlsequence.y - controlsequence.y)
                        theta_norm = np.linalg.norm(test_controlsequence.theta - controlsequence.theta)
                        u_norm = np.linalg.norm(test_controlsequence.u - controlsequence.u)
                        print("x_norm: " + str(x_norm/self.N))
                        print("y_norm: " + str(y_norm/self.N))
                        print("theta_norm: " + str(theta_norm/self.N))
                        print("u_norm: " + str(u_norm/self.N))
                        print("duplicate found!")

                        duplicate = True
                        break



        return duplicate
    def flip_control_sequence_about_x(self, controlsequence):

        duplicate = False
        flipped_controlsequence = ControlSequence(self.N)

        if(np.all(np.abs(controlsequence.y) < 1e-9)):
            duplicate = True
            print("DUPLICATE")
        else:
            vector_wrap_orientation = np.vectorize(wrap_orientation)
            flipped_controlsequence.x = controlsequence.x
            flipped_controlsequence.y = -controlsequence.y
            flipped_controlsequence.theta = -controlsequence.theta
            flipped_controlsequence.u = -controlsequence.u
            flipped_controlsequence.T = controlsequence.T
            flipped_controlsequence.v = controlsequence.v
            flipped_controlsequence.s = controlsequence.s
            flipped_controlsequence.alpha = -controlsequence.alpha

        return duplicate, flipped_controlsequence

    def reverse_controlsequence(self, controlsequence):
        reverse_controlsequence = ControlSequence(self.N)
        reverse_controlsequence.x = np.flip(controlsequence.x) - controlsequence.x[-1]
        reverse_controlsequence.y = np.flip(controlsequence.y) - controlsequence.y[-1]
        reverse_controlsequence.theta = np.flip(controlsequence.theta)
        reverse_controlsequence.alpha = np.flip(controlsequence.alpha)
        reverse_controlsequence.u = np.flip(controlsequence.u)
        reverse_controlsequence.T = controlsequence.T
        reverse_controlsequence.v = -controlsequence.v
        reverse_controlsequence.s = controlsequence.s

        return reverse_controlsequence

    def generate_reduced_control_sequences(self):

        optimize_args = []
        alpha = [-0.2117, 0.0, 0.2117]
        theta_S = [0, pi / 8, pi / 4 ]
        theta_E = [0, pi / 8, pi / 4, 3*pi/8, pi / 2]

        M = 1
        start = 0.0
        end = 15.0
        ds = (end - start) / M

        Y = np.arange(start, end + ds, ds)
        X = [ds]

        for theta_s in theta_S:
            for alpha_s in alpha:
                for x_e in X:
                    for y_e in Y:
                        for theta_e in theta_E:
                            for alpha_e in alpha:
                                optimize_args.append((0.0, 0.0, theta_s, alpha_s, x_e, y_e, theta_e, alpha_e, self.optimizer))

        start = time.time()
        with Pool(20) as pool:
            results = pool.map(self.optimize_wrapper, optimize_args)
        end = time.time()
        print("elapsed time: " + str(end - start))

        for i, (solve_flag, ref_x, ref_y, ref_theta, ref_alpha, u, T) in enumerate(results):

            if solve_flag:
                controlsequence = ControlSequence(self.N)
                controlsequence.x = ref_x
                controlsequence.y = ref_y

                plt.plot(ref_x, ref_y)

                diff_x = np.diff(ref_x)
                diff_y = np.diff(ref_y)

                diff_x_2 = np.power(diff_x, 2)
                diff_y_2 = np.power(diff_y, 2)

                controlsequence.s = np.sum(np.sqrt(np.add(diff_x_2, diff_y_2)))

                controlsequence.theta = ref_theta
                controlsequence.alpha = ref_alpha
                controlsequence.u = u
                controlsequence.T = T
                controlsequence.v = 1.0

                self.controlsequences.append(controlsequence)

                duplicate, flipped_controlsequence = self.flip_control_sequence_about_x(controlsequence)
                if not duplicate:
                    self.controlsequences.append(flipped_controlsequence)
                    plt.plot(flipped_controlsequence.x, flipped_controlsequence.y)

                reverse_controlsequence = self.reverse_controlsequence(controlsequence)
                self.controlsequences.append(reverse_controlsequence)

                duplicate, flipped_reverse_controlsequence = self.flip_control_sequence_about_x(reverse_controlsequence)
                if not duplicate:
                    self.controlsequences.append(flipped_reverse_controlsequence)

        print(len(self.controlsequences))
        input("PAUSE")

        with open('reduced_controlsequences_with_alpha.pkl', 'wb') as f:
            pickle.dump(self.controlsequences, f)

    def plot_sequences(self):
        with open('reduced_controlsequences_with_alpha.pkl', 'rb') as f:
            self.controlsequences = pickle.load(f)

        for controlsequence in self.controlsequences:
            plt.plot(controlsequence.x, controlsequence.y, color='blue')

        plt.show()

    def generate_control_sequences(self):

        optimize_args = []
        alpha = [0.0]
        theta = [0, pi / 8, pi / 4, 3*pi/8, pi / 2]

        M = 1
        start = 0.0
        end = 15.0
        ds = (end - start) / M

        Y = np.arange(start, end + ds, ds)
        X = [ds]

        for theta_s in theta:
            for alpha_s in alpha:
                for x_e in X:
                    for y_e in Y:
                        for theta_e in theta:
                            for alpha_e in alpha:
                                optimize_args.append((0.0, 0.0, theta_s, alpha_s, x_e, y_e, theta_e, alpha_e, self.optimizer))

        start = time.time()
        with Pool(20) as pool:
            results = pool.map(self.optimize_wrapper, optimize_args)
        end = time.time()
        print("elapsed time: " + str(end - start))

        for i, (solve_flag, ref_x, ref_y, ref_theta, ref_alpha, u, T) in enumerate(results):
            print("solve_flag: " + str(solve_flag))

            if solve_flag:
                dt = T / self.N
                controlsequence = ControlSequence(self.N)
                controlsequence.x = ref_x
                controlsequence.y = ref_y

                plt.plot(ref_x, ref_y, color='blue')

                diff_x = np.diff(ref_x)
                diff_y = np.diff(ref_y)

                diff_x_2 = np.power(diff_x, 2)
                diff_y_2 = np.power(diff_y, 2)

                controlsequence.s = np.sum(np.sqrt(np.add(diff_x_2, diff_y_2)))

                controlsequence.theta = ref_theta
                controlsequence.alpha = ref_alpha
                controlsequence.u = u
                controlsequence.T = T
                controlsequence.v = 1.0

                self.controlsequences.append(controlsequence)

                reverse_controlsequence = ControlSequence(self.N)
                reverse_controlsequence.x = np.flip(ref_x) - ref_x[-1]
                reverse_controlsequence.y = np.flip(ref_y) - ref_y[-1]
                reverse_controlsequence.theta = np.flip(ref_theta)
                reverse_controlsequence.u = np.flip(u)
                reverse_controlsequence.T = T
                reverse_controlsequence.v = -1.0
                reverse_controlsequence.s = controlsequence.s

                self.controlsequences.append(reverse_controlsequence)

    def check_control_sequence_theta(self):
        for controlsequence in self.controlsequences:
            print("controlsequence.theta[0]: " + str(controlsequence.theta[0]))
            print("controlsequence.theta[-1]: " + str(controlsequence.theta[-1]))

    def reflect_control_sequences(self):
        reflected_controlsequences = []
        vector_wrap_orientation = np.vectorize(wrap_orientation)

        for controlsequence in self.controlsequences:
            reflected_controlsequence = ControlSequence(self.N)
            reflected_controlsequence.x = -controlsequence.y
            reflected_controlsequence.y = controlsequence.x
            reflected_controlsequence.s = controlsequence.s
            reflected_controlsequence.theta = vector_wrap_orientation(controlsequence.theta+pi/2)
            reflected_controlsequence.u = controlsequence.u
            reflected_controlsequence.T = controlsequence.T
            reflected_controlsequence.v = controlsequence.v
            reflected_controlsequence.alpha = controlsequence.alpha
            duplicate = self.check_for_duplicates(reflected_controlsequence, self.controlsequences + reflected_controlsequences)
            if not duplicate:
                reflected_controlsequences.append(reflected_controlsequence)
                plt.plot(reflected_controlsequence.x , reflected_controlsequence.y , color='red')

        for controlsequence in self.controlsequences:
            reflected_controlsequence = ControlSequence(self.N)
            reflected_controlsequence.x = -controlsequence.x
            reflected_controlsequence.y = -controlsequence.y
            reflected_controlsequence.s = controlsequence.s
            reflected_controlsequence.theta = vector_wrap_orientation(controlsequence.theta+pi)
            reflected_controlsequence.u = controlsequence.u
            reflected_controlsequence.T = controlsequence.T
            reflected_controlsequence.v = controlsequence.v
            reflected_controlsequence.alpha = controlsequence.alpha
            duplicate = self.check_for_duplicates(reflected_controlsequence, self.controlsequences + reflected_controlsequences)
            if not duplicate:
                reflected_controlsequences.append(reflected_controlsequence)
                plt.plot(reflected_controlsequence.x, reflected_controlsequence.y, color='green')

        for controlsequence in self.controlsequences:
            reflected_controlsequence = ControlSequence(self.N)
            reflected_controlsequence.x = controlsequence.y
            reflected_controlsequence.y = -controlsequence.x
            reflected_controlsequence.s = controlsequence.s
            reflected_controlsequence.theta = vector_wrap_orientation(controlsequence.theta - pi/2)
            reflected_controlsequence.u = controlsequence.u
            reflected_controlsequence.T = controlsequence.T
            reflected_controlsequence.v = controlsequence.v
            reflected_controlsequence.alpha = controlsequence.alpha
            duplicate = self.check_for_duplicates(reflected_controlsequence, self.controlsequences + reflected_controlsequences)
            if not duplicate:
                reflected_controlsequences.append(reflected_controlsequence)
                plt.plot(reflected_controlsequence.x, reflected_controlsequence.y, color='orange')

        self.controlsequences = self.controlsequences + reflected_controlsequences

        plt.show()

        with open('controlsequences.pkl', 'wb') as f:
            pickle.dump(self.controlsequences, f)

        print(len(self.controlsequences))

    def generate_search_space(self):
        n_grid = 3
        x_lattice = range(-n_grid * 15, (n_grid + 1) * 15, 15)
        y_lattice = x_lattice
        yaw_lattice = [-pi / 2, 0, pi / 2, pi]

        for x in x_lattice:
            for y in y_lattice:
                for yaw in yaw_lattice:
                    for controlsequence in self.controlsequences:
                        node_exists = False
                        dx = controlsequence.x[-1]
                        dy = controlsequence.y[-1]
                        theta_s = wrap_orientation(controlsequence.theta[0] + yaw)
                        theta_e = wrap_orientation(controlsequence.theta[-1] + yaw)
                        for node in self.graph.Nodes:
                            if node.x == x and node.y == y and node.yaw == theta_s:
                                node_exists = True
                                break

                        if not node_exists:
                            node = Node(x, y, theta_s, 0.0)
                            self.graph.add_node(node)

                        yaw_child = theta_e
                        x_child = node.x + dx * cos(yaw) - dy * sin(yaw)
                        y_child = node.y + dx * sin(yaw) + dy * cos(yaw)

                        child_node = Node(x_child, y_child, yaw_child, 0.0, node.index)
                        self.graph.add_node(child_node, controlsequence.s, 1.0, controlsequence.u, controlsequence.T)

    def plot_search_space(self):
        for i in range(0, len(self.graph.Nodes)):
            for j in range(0, len(self.graph.Nodes[i].children)):
                model = TruckTrailerModel()
                beta3_s, beta2_s = self.optimizer.get_beta(self.graph.Nodes[i].alpha)
                model._states[0] = self.graph.Nodes[i].x
                model._states[1] = self.graph.Nodes[i].y
                model._states[2] = self.graph.Nodes[i].yaw
                model._states[3] = beta3_s
                model._states[4] = beta2_s
                model._states[5] = self.graph.Nodes[i].alpha
                u = self.graph.Nodes[i].edges[j].u
                dt = self.graph.Nodes[i].edges[j].T / self.N
                model.set_simulation(0.0, 0.0, u, dt)
                controlsequence = ControlSequence(self.N)
                for k in range(0, self.N):
                    model.forward_euler(1.0, u[k])
                    controlsequence.x[k] = model._states[0]
                    controlsequence.y[k] = model._states[1]

                ax.plot(controlsequence.x[::2], controlsequence.y[::2], color='r', linewidth=0.1)


    def find_shortest_path(self, x_0, y_0, yaw_0, alpha_0, x_f, y_f, yaw_f, alpha_f):
        t = time.time()
        self.graph.astar(x_0, y_0, yaw_0, alpha_0, x_f, y_f, yaw_f, alpha_f, self.controlsequences)
        elapsed = time.time() - t
        index_end, error_end = self.graph.find_closest_node(x_f, y_f, yaw_f, alpha_f)
        self.index_end = index_end
        print("Elapsed time is " + str(elapsed))


    def plot_shortest_path(self, index):

        self.optimal_node_traversal = [index]
        print("Final cost: " + str(self.graph.Nodes[index].cost))

        while not isnan(self.graph.Nodes[index].previous_node_index):
            prev_i = self.graph.Nodes[index].previous_node_index
            for edge in self.graph.Nodes[prev_i].edges:
                if edge.i is index:
                    self.optimal_x = np.insert(self.optimal_x, 0, edge.x, axis=0)
                    self.optimal_y = np.insert(self.optimal_y, 0, edge.y, axis=0)
                    plt.plot(edge.x, edge.y, color='r')
                    plt.show(block=False)
                    plt.pause(0.01)
                    input("PAUSE")

            index = self.graph.Nodes[index].previous_node_index
            self.optimal_node_traversal.insert(0, index)

    def plot_visited_nodes(self):
        for index, node in enumerate(self.graph.visited_list):
            for edge in node.edges:
                model = TruckTrailerModel()
                beta3_s, beta2_s = self.optimizer.get_beta(node.alpha)
                model._states[0] = node.x
                model._states[1] = node.y
                model._states[2] = node.yaw
                model._states[3] = beta3_s
                model._states[4] = beta2_s
                model._states[5] = node.alpha
                u = edge.u
                dt = edge.T / self.N
                model.set_simulation(0.0, 0.0, u, dt)
                controlsequence = ControlSequence(self.N)
                for k in range(0, self.N):
                    model.forward_euler(1.0, u[k])
                    controlsequence.x[k] = model._states[0]
                    controlsequence.y[k] = model._states[1]

                ax.plot(controlsequence.x[::2], controlsequence.y[::2], color='g', linewidth=0.1)

    def simulate_path(self):
        for i in range(len(self.optimal_node_traversal) - 1):
            model = TruckTrailerModel()
            beta3_s, beta2_s = self.optimizer.get_beta(self.graph.Nodes[self.optimal_node_traversal[i]].alpha)
            model._states[0] = self.graph.Nodes[self.optimal_node_traversal[i]].x
            model._states[1] = self.graph.Nodes[self.optimal_node_traversal[i]].y
            model._states[2] = self.graph.Nodes[self.optimal_node_traversal[i]].yaw
            model._states[3] = beta3_s
            model._states[4] = beta2_s
            model._states[5] = self.graph.Nodes[self.optimal_node_traversal[i]].alpha
            for edge in self.graph.Nodes[self.optimal_node_traversal[i]].edges:
                if edge.i is self.optimal_node_traversal[i + 1]:
                    u = edge.u
                    dt = edge.T / self.N
                    model.set_simulation(self.optimal_x, self.optimal_y, u, dt)
                    controlsequence = ControlSequence(self.N)
                    for k in range(0, self.N):
                        model.forward_euler(edge.v, u[k], True)
                        controlsequence.x[k] = model._states[0]
                        controlsequence.y[k] = model._states[1]

    def simulate_backwards_path(self):
        for i in reversed(range(len(self.optimal_node_traversal))):
            print(i)
            model = TruckTrailerModel()
            beta3_s, beta2_s = self.optimizer.get_beta(self.graph.Nodes[self.optimal_node_traversal[i]].alpha)
            model._states[0] = self.graph.Nodes[self.optimal_node_traversal[i]].x
            model._states[1] = self.graph.Nodes[self.optimal_node_traversal[i]].y
            model._states[2] = self.graph.Nodes[self.optimal_node_traversal[i]].yaw
            model._states[3] = beta3_s
            model._states[4] = beta2_s
            model._states[5] = self.graph.Nodes[self.optimal_node_traversal[i]].alpha
            for edge in self.graph.Nodes[self.optimal_node_traversal[i-1]].edges:
                if edge.i is self.optimal_node_traversal[i]:
                    u = edge.u
                    dt = edge.T / self.N
                    model.set_simulation(self.optimal_x, self.optimal_y, u, dt)
                    controlsequence = ControlSequence(self.N)
                    for k in reversed(range(0, self.N)):
                        model.forward_euler(-1.0, u[k], True)
                        controlsequence.x[k] = model._states[0]
                        controlsequence.y[k] = model._states[1]

if __name__ == '__main__':
    truck_trailer_lattice_planner = TruckTrailerLatticePlanner()
    # truck_trailer_lattice_planner.generate_reduced_control_sequences()
    # truck_trailer_lattice_planner.reflect_control_sequences()
    with open('controlsequences.pkl', 'rb') as f:
        truck_trailer_lattice_planner.controlsequences = pickle.load(f)

    x_0 = 0.0
    y_0 = 0.0
    yaw_0 = 0.0

    x_f = 45.0
    y_f = 0.0
    yaw_f = pi/8

    truck_trailer_lattice_planner.find_shortest_path(x_0, y_0, yaw_0, 0.0, x_f, y_f, yaw_f, 0.0)
    truck_trailer_lattice_planner.plot_shortest_path(truck_trailer_lattice_planner.index_end)
    plt.show()
    plt.pause(0.01)
    input("PAUSE")
    truck_trailer_lattice_planner.simulate_path()

