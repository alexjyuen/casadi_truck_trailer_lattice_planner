from casadi import *
import math

class TruckTrailerOptimizer():
    def __init__(self, M1 = 0.8, L1 = 4.66, L2 = 3.75, L3 = 7.59, N = 100, beta3_max = pi/2, beta3_min = -pi/2,
                 beta2_max = pi/2, beta2_min = -pi/2, alpha_max = pi/4, alpha_min = -pi/4, omega_max = 1.5, omega_min = -1.5,
                 u_max = 40, u_min = -40):

        self.M1 = M1
        self.L1 = L1
        self.L2 = L2
        self.L3 = L3
        self.N = N

        self.beta3_max = beta3_max
        self.beta3_min = beta3_min

        self.beta2_max = beta2_max
        self.beta2_min = beta2_min

        self.alpha_max = alpha_max
        self.alpha_min = alpha_min

        self.omega_max = omega_max
        self.omega_min = omega_min

        self.u_max = u_max
        self.u_min = u_min

    def run_optimizer(self, x_start, y_start, theta_start, alpha_start, x_end, y_end, theta_end, alpha_end):
        opti = Opti() # Optimization problem

        X = opti.variable(7, self.N+1)

        x3 = X[0,:]
        y3 = X[1,:]
        theta3 = X[2,:]
        beta3 = X[3,:]
        beta2 = X[4,:]
        alpha = X[5,:]
        omega = X[6,:]

        U = opti.variable(1,self.N)   # control trajectory (throttle)
        T = opti.variable()      # final time
        opti.minimize(U @ U.T + omega @ omega.T * 10 + T)

        # ---- dynamic constraints --------
        f = lambda x,u: vertcat(cos(x[3])*cos(x[4])*(1+self.M1/self.L1*tan(x[4])*tan(x[5]))*cos(x[2]),
                                cos(x[3])*cos(x[4])*(1+self.M1/self.L1*tan(x[4])*tan(x[5]))*sin(x[2]),
                                ((sin(x[3])*cos(x[4]))/self.L3)*(1+self.M1/self.L1*tan(x[4])*tan(x[5])),
                                cos(x[4])*((1/self.L2)*(tan(x[4])-(self.M1/self.L1)*tan(x[5]))-(sin(x[3])/self.L3)*(1+(self.M1/self.L1)*tan(x[4])*tan(x[5]))),
                                (tan(x[5])/self.L1 - sin(x[4])/self.L2 + self.M1/(self.L1*self.L2)*cos(x[4])*tan(x[5])),
                                x[6],
                                u[0]
                                )

        dt = T/self.N # length of a control interval
        for k in range(self.N): # loop over control intervals
           # Runge-Kutta 4 integration
           k1 = f(X[:,k],         U[:,k])
           k2 = f(X[:,k]+dt/2*k1, U[:,k])
           k3 = f(X[:,k]+dt/2*k2, U[:,k])
           k4 = f(X[:,k]+dt*k3,   U[:,k])
           x_next = X[:,k] + dt/6*(k1+2*k2+2*k3+k4)
           opti.subject_to(X[:,k+1]==x_next) # close the gaps

        opti.subject_to(beta3 <= self.beta3_max)
        opti.subject_to(beta3 >= self.beta3_min)

        opti.subject_to(beta2 <= self.beta2_max)
        opti.subject_to(beta2 >= self.beta2_min)

        opti.subject_to(alpha <= self.alpha_max)
        opti.subject_to(alpha >= self.alpha_min)

        opti.subject_to(omega <= self.omega_max)
        opti.subject_to(omega >= self.omega_min)

        opti.subject_to(T>=0)

        opti.subject_to(U <= self.u_max)
        opti.subject_to(U >= self.u_min)

        beta3_start, beta2_start = self.get_beta(alpha_start)
        beta3_end, beta2_end = self.get_beta(alpha_end)

        opti.subject_to(x3[0] == x_start)   # start at position 0 ...
        opti.subject_to(y3[0] == y_start) # ... from stand-still
        opti.subject_to(theta3[0] == theta_start)  # finish line at position 1
        opti.subject_to(beta3[0] == beta3_start)   # start at position 0 ...
        opti.subject_to(beta2[0] == beta2_start) # ... from stand-still
        opti.subject_to(alpha[0] == alpha_start)  # finish line at position 1
        opti.subject_to(omega[0] == 0)  # finish line at position 1

        opti.subject_to(x3[-1] == x_end)   # start at position 0 ...
        opti.subject_to(y3[-1] == y_end) # ... from stand-still
        opti.subject_to(theta3[-1] == theta_end)  # finish line at position 1
        opti.subject_to(beta3[-1] == beta3_end)   # start at position 0 ...
        opti.subject_to(beta2[-1] == beta2_end) # ... from stand-still
        opti.subject_to(alpha[-1] == alpha_end)  # finish line at position 1
        opti.subject_to(omega[-1] == 0)  # finish line at position 1

        options = {
            'ipopt': {
                'tol': 1e-3,
                'dual_inf_tol': 1e-2,
                'constr_viol_tol': 1e-2,
                'compl_inf_tol': 1e-2,
                'hessian_approximation': 'limited-memory',
                'print_level': 0,
                'linear_solver': 'mumps',
                'mu_strategy': 'adaptive',
                'max_iter': 2000,
            }
        }

        opti.solver("ipopt", options) # set numerical backend

        try:
            sol = opti.solve()   # actual solve
        except RuntimeError:
            return 0, 0, 0, 0, 0, 0, 0

        return 1, sol.value(x3), sol.value(y3), sol.value(theta3), sol.value(alpha), sol.value(U), sol.value(T)

    def get_R(self, alpha):

        if fabs(alpha) > 0:
            R1 = self.L1/fabs(tan(alpha))
            R2 = sqrt(R1*R1 + self.M1*self.M1 - self.L2*self.L2)
            R3 = sqrt(R2*R2 - self.L3*self.L3)
        else:
            R1 = float('inf')
            R2 = R1
            R3 = R1

        return R1, R2, R3

    def get_beta(self, alpha):
        R1, R2, R3 = self.get_R(alpha)

        if(math.isinf(R1)):
            beta3 = 0.0
            beta2 = 0.0
        else:
            beta3 = copysign(1, alpha) * atan(self.L3 / R3)
            beta2 = copysign(1, alpha) * (atan(self.M1 / R1) + atan(self.L2 / R2))

        return beta3, beta2

if __name__ == '__main__':
    truck_trailer_optimizer = TruckTrailerOptimizer()